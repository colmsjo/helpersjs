# helpers

My collection of JavaScript helpers. Mainly used when developing. Use a proper CDN
for libraries when deploying to production.

This repo also contain my own helpers in lib/js/helpers.js and lib/js/simpleAsync.js


## Getting Started

See the tests for howto use the libraries.

In node, start with installing using npm

```
npm install
```


## Development

Just do `grunt nodeunit` for the node tests.

Open test/helpers_test.html in a browser to run the qunit tests.

There is also a Makefile:

 * `make test` - run nodeunit tests
 * `make asynctest` - simple test of simpleAsync.js
 * `make doc` - build documentation
 * `make build` - generate minified helpers
 * `make clean` - clean up

