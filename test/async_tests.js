/*jshint -W054, evil:true, devel:true*/

var helpers = require('../build/helpersjs.min.js').create();
helpers.logging_threshold = helpers.logging.debug;

var async = require('../build/helpersjs.min.js'); 

var asyncLibFactory = function(asyncObj) {
  return {
      a   : asyncObj,
      i   : 2,
      inc : function(x){ this.a.stopped(); return x+this.i;},
      sq  : function(x){ this.a.stopped(); return x*x;},
      I   : function(x){ this.a.stopped(); return x;}
    };
};


// --------------------------------------------------------
// See console for output

var asyncObj = async.createAsync(helpers);
var asyncLib = asyncLibFactory(asyncObj);

asyncObj.firstThen(
        function() { asyncObj.delayed(2, asyncLib.sq.bind(asyncLib)); },
        function() { asyncObj.stopped(); console.log('A. firstThen, should see 4 next: ' + asyncObj.getResult() ); }
      );

console.log('1. firstThen, will likely see null next: ' + asyncObj.getResult());

asyncObj.delayed(
  null, 
  function(x) { 
    console.log('B. Waited 2 secs. output should be ready: expect 4: ' + 
      asyncObj.getResult());
  },
  2000);


// --------------------------------------------------------
// See console for output

var asyncObj2 = async.createAsync(helpers);
var asyncLib2 = asyncLibFactory(asyncObj2);

asyncObj2.firstThen(
        function() { asyncObj2.delayed(2, asyncLib2.sq.bind(asyncLib2)); },
        function() { asyncObj2.delayed(6, asyncLib2.inc.bind(asyncLib2)); }
      );

console.log('2. firstThen, will likely see null next: ' + asyncObj2.getResult());


asyncObj2.delayed(
  null, 
  function(x) {
    asyncObj2.stopped();
    console.log('C. Waited 4 secs. output should be ready: expect 4,8: ' + 
      asyncObj2.getResult());
  },
  4000);


// --------------------------------------------------------
// See console for output

var asyncObj3 = async.createAsync(helpers);
var asyncLib3 = asyncLibFactory(asyncObj3);

asyncObj3.series([
  function() { asyncObj3.delayed(null, function() { asyncObj3.stopped(); console.log('D. Testing async.series function'); }, 5000); },
  function() { asyncObj3.delayed(2, asyncLib3.sq.bind(asyncLib3)); },
  function() { asyncObj3.delayed(6, asyncLib3.inc.bind(asyncLib3)); },
  function() { 
    asyncObj3.delayed(
      null, 
      function(x) {
        asyncObj3.stopped();
        console.log('E. Waited 4 secs. output should be ready: expect 4,8: ' + 
          asyncObj3.getResult());
      },
      4000);
  }
  ]);



// --------------------------------------------------------
// Using the caolan library

var a = require('async');
a.series([
  function(fn){setTimeout(function() {console.log('hej ');fn(null,'one');}, 1000); }, 
  function(fn){setTimeout(function() {console.log('då!');fn(null,'two');}, 1000); }
  ]);

a.series([
  function(fn){setTimeout(function() {
    a.series([
      function(fn) {}
    ]);
    console.log('hej ');fn(null,'one');}, 1000); 
  }, 
  function(fn){setTimeout(function() {console.log('då!');fn(null,'two');}, 1000); }
  ]);

