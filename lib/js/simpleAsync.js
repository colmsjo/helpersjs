/*jshint browser:true, devel:true, laxcomma:true*/
(function(exports) {

// simpleAsync.js
//------------------------------
//
// 2013-07-26, Jonas Colmsjö
//
// Copyright 2013 Gizur AB 
//
// Simple helper for async development
//
// dependencies: None
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
// The code is commented in 'docco style' - http://jashkenas.github.com/docco/ 
//
//
// Use in browser:
//---------------
// <script type="text/javascript" src="https://raw.github.com/colmsjo/helpersjs/master/lib/js/simpleAsync.js"></script>
// ...
// var h = simpleAsync.create();
// h.firstThen
// ...
//
// Use in node:
// var async = require('./path/simpleAsync.js').create();
// async.firstThen( function() {...}, function(){...} )
//
//------------------------------

    "use strict";

    exports.createAsync = function(helpers, timout_intervall) {

        var tmpHelper           = (helpers !== undefined) ? 
                                    helpers 
                                    : require( __dirname + '/helpers.js' ).create();

        var tmpTimeoutIntervall = (timout_intervall !== undefined) ? 
                                    timout_intervall 
                                    : 200;

        return {
            _helpers          : tmpHelper,
            _asyncRunning     : false,
            _timeoutIntervall : tmpTimeoutIntervall,
            _result           : [],
            _counter          : 0,
            _intervalId       : null,

            started   : function() {
                this._helpers.logDebug('simpleAsync.started');
                if(this._asyncRunning) {
                    return new Error('simpleAsync.started: started called on async object that already is started');
                }
                this._asyncRunning = true;
            },
            stopped   : function() {
                this._helpers.logDebug('simpleAsync.stopped');
                if(!this._asyncRunning) {
                    return new Error('simpleAsync.stopped: stopped called on async object that already is stopped');
                }
                this._asyncRunning = false;
             },
            getResult : function() {return (!this._asyncRunning) ? this._result : null;},

            // Wait until first() has completed and run then() and return without waiting
            firstThen : function (first, then) {

                // reset the result
                //this._result = [];

                // start the first function, this function must call stopped
                this.started();
                first();

                // check if first() has finished periodically
                this._intervalId = setInterval(
                    function() {
                        if(this._asyncRunning) {
                            this._counter++;
                            this._helpers.logDebug('simpleAsync.firstThen: counter - ' + this._counter);
                        }
                        else {
                            this._helpers.logDebug('simpleAsync.firstThen: this._asyncRunning now false. ' + this._intervalId);
     
                             // stop the checking
                            clearInterval(this._intervalId);

                            // start the second function and return (we will not wait for this to finish)
                            this.started();
                            then();
                        }
                    }.bind(this)
                    , this._timeoutIntervall);
            },

            // just a way to construct a async function, mainly used for testig, 
            delayed : function(args, callback, milliseconds) {
                this._helpers.logDebug('simpleAsync.delayed: start of function');
                if(milliseconds === undefined) {
                    milliseconds = 1000;
                }
                setTimeout(
                    function() {
                        //this.stopped();
                        this._result.push(callback(args));
                    }.bind(this)
                    , milliseconds);
            },

            // runs series of async functions
            //
            // Pseudo cocde:
            // Series(h::tail) = firstThen(h, Series(tail))
            // Series(h::t::nil) = firstThen(h,t)
            series : function(funcs) {
                this._helpers.logDebug('simpleAsync.series: start of function');
                if(funcs.length < 2) {
                    return new Error("At least two functions are needed!");
                }
                if(funcs.length === 2) {
                    this._helpers.logDebug('simpleAsync.series: funcs.length === 2');
                    this.firstThen(funcs[0], funcs[1]);
                } else {
                    this._helpers.logDebug('simpleAsync.series: funcs.length > 2');
                    this.firstThen(funcs[0], this.series(funcs.slice(1,funcs.length)));
                }
            }

        };
    };

}(typeof exports === 'undefined' ? this['simpleAsync']={} : exports));