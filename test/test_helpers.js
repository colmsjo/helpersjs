/*jshint -W054, evil:true, devel:true, browser:true*/

// the functions to test
var helpers = require('../lib/js/helpers.js').create();

//
// Unit tests for the helper functions that are executed from node
//
// Run like this:
// >nodeunit helpers_test.js
//

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

// Unit test
//-------------------------------------------------------------------------------------------------
// Basic tests that just calls the functions to make sure that execute ok

exports['test_helpersjs'] = {

  setUp: function(done) {
    // setup here

    // setup finished
    done();
  },

  'misc functions': function(test) {

    // There should be X tests
    test.expect(9);

    // tests here
    test.equal(true,  helpers.isEmptyObject({}),      'isEmptyObject');
    test.equal(false, helpers.isEmptyObject({a:'a'}), 'isEmptyObject');

    test.equal(false, helpers.isset(""), 'isset("")');
    test.equal(true,  helpers.isset("a"), 'isset("a")');

    test.equal(false, helpers.isset([]), 'isset("[]")');
    test.equal(true,  helpers.isset([1]), 'isset("[1]")');

    test.equal(false, helpers.isset(undefined), 'isset("undefined")');
    test.equal(false, helpers.isset(null), 'isset("null")');

    test.equal(true, helpers.isset(1), 'isset("1")');

    // All tests performed
    test.done();
  },

  'logging functions': function(test) {

    // There should be X tests
    test.expect(8);

    // tests here
    helpers.logging_threshold  = helpers.logging.debug;
    test.equal(undefined, helpers.logEmerg('\nlogEmerg statement...'), 'logEmerg');
    test.equal(undefined, helpers.logAlert('logAlert statement...'), 'logAlert');
    test.equal(undefined, helpers.logCrit('logCrit statement...'), 'logCrit');
    test.equal(undefined, helpers.logErr('logErr statement...'), 'logErr');
    test.equal(undefined, helpers.logWarning('logWarning statement...'), 'logWarning');
    test.equal(undefined, helpers.logNotice('logNotice statement...'), 'logNotice');
    test.equal(undefined, helpers.logInfo('logInfo statement...'), 'logInfo');
    test.equal(undefined, helpers.logDebug('logDebug statement...'), 'logDebug');

    // All tests performed
    test.done();
  },

  'API Keys and signing': function(test) {

    // There should be X tests
    test.expect(2);

//    var bigint       = require('../node_modules/BigInt/src/BigInt.js');
    var bigint       = require('BigInt');
    var keyAndSecret = helpers.generateApiKeyAndSecret(bigint);
    test.equal(undefined, helpers.logDebug('generateApiKeyAndSecret: ' + JSON.stringify(keyAndSecret)), 'generateApiKeyAndSecret');

    //var sha       = require('../lib/js/node-sha256.js');
    var signature = helpers.sign('HelpDesk', 'GET', '45VJ9SPF4SDHD', 'LbOhdTRQHYjEjYhIgZeF', 3600*2);
    test.equal(undefined, helpers.logDebug('generateApiKeyAndSecret: ' + signature), 'sign');

    // All tests performed
    test.done();
  }

};
